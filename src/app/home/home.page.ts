import { HttpClient } from '@angular/common/http';
import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit, OnDestroy, AfterViewInit {

  nome: string;
  Cognome = 'Iannuccillo';
  mostra = false;

  elementi: string[] = ['Antonio', 'Domenico'];
  constructor(
    private router: Router,
    private toast: ToastController,
    private http: HttpClient
    ) {
    }
  ngOnInit() {
    this.nome = 'Antonio';

    // this.elementi = null;

    this.http.get<any>('https://swapi.dev/api/people/').subscribe(
      (res) => {
        const risultati = res.results; // array di nomi
        for (const personaggio of risultati) {
          // console.log(personaggio);
          console.log(personaggio.name + ' - ' + personaggio.hair_color);
          this.elementi.push(personaggio.name);
        }
      }
    );
  }
  ngOnDestroy() {

  }
  ngAfterViewInit() {
  }
  public vai(indirizzo: string, direzione: string): string {
    this.router.navigate (['/login']);
    return '';
  }
  public andiamo() {
    this.router.navigate(['/sito1']);
  }
  onCambia() {
    if (this.nome === 'Antonio') {
      this.nome = 'Domenico';
      this.Cognome = 'Carlini';
    } else {
      this.nome = 'Antonio';
      this.Cognome = 'Iannuccillo';
    }
  }
  onLogin(nome: any) {
    console.log(nome);
    console.log('il cognome ora è ' + this.Cognome);
    this.nome = nome.value;
  }
  onChange() {
    console.log('Cambiato');
  }
}
