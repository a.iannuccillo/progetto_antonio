import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { StudentiPageRoutingModule } from './studenti-routing.module';

import { StudentiPage } from './studenti.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    StudentiPageRoutingModule
  ],
  declarations: [StudentiPage]
})
export class StudentiPageModule {}
