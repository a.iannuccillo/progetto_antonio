import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-studenti',
  templateUrl: './studenti.page.html',
  styleUrls: ['./studenti.page.scss'],
})
export class StudentiPage implements OnInit {

  studente = {
  matricola: 162618, // coppia (chiave: valore)
  nome: 'Gianni',
  cognome: 'grazieAssai',
  cds: 'info'
  }
  constructor() { }

  ngOnInit() {
  }

}
