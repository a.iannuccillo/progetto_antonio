import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { StudentiPage } from './studenti.page';

const routes: Routes = [
  {
    path: '',
    component: StudentiPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StudentiPageRoutingModule {}
