import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Sito1PageRoutingModule } from './sito1-routing.module';

import { Sito1Page } from './sito1.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Sito1PageRoutingModule
  ],
  declarations: [Sito1Page]
})
export class Sito1PageModule {}
