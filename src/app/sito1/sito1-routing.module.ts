import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Sito1Page } from './sito1.page';

const routes: Routes = [
  {
    path: '',
    component: Sito1Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Sito1PageRoutingModule {}
