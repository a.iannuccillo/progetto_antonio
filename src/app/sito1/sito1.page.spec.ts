import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Sito1Page } from './sito1.page';

describe('Sito1Page', () => {
  let component: Sito1Page;
  let fixture: ComponentFixture<Sito1Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Sito1Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Sito1Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
